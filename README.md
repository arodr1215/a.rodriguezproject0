README:
Hello, my name is Anthony Rodriguez, and this is my project 0. This is a
Banking application that would have stored customer and employee data and
performed a variety of banking related functions, such as depositing and
withdrawing funds. It would have done so by utilizing JDBC, DAO classes,
Log4j and JUnit. I attempted to the best of my ability to complete this on
time, however I was not able to implement the code. I tried too grand a scope at
first when I should have been implementing small segments of the code first,
testing to make sure they worked, then continued to expand. All of the classes
that are necessary were created, either completed or partially developed but since
I wasn't able to finish the DAO methods as well as the drivers, I never got to test
their functionality.
