package dev.rodriguez.models;

public class BankCustomer extends BankUser 
{
	protected final int customerNum = 1;
	protected String socialSecurity;
	
	public BankCustomer()
	{
		super();
	}
	
	public BankCustomer(String socialSecurity) {
		this.socialSecurity = socialSecurity;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((socialSecurity == null) ? 0 : socialSecurity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BankCustomer other = (BankCustomer) obj;
		if (socialSecurity == null) {
			if (other.socialSecurity != null)
				return false;
		} else if (!socialSecurity.equals(other.socialSecurity))
			return false;
		return true;
	}

	public String getSocialSecurity() {
		return socialSecurity;
	}

	public void setSocialSecurity(String socialSecurity) {
		this.socialSecurity = socialSecurity;
	}
	
	public int getCustomerNum()
	{
		return customerNum;
	}
	
	public int userType()
	{
		return customerNum; 
	}
	
	public String details()
	{
		return "This user is a customer, their name is " + firstName + " " + lastName;
	}
	
	
}
