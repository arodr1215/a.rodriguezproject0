package dev.rodriguez.models;

public class BankEmployee extends BankUser
{
	protected final int employeeNum = 3;
	protected String employeeID;
	
	public BankEmployee()
	{
		super();
	}
	
	public BankEmployee(String employeeID)
	{
		this.employeeID = employeeID;
	}
	
	public int userType()
	{
		return employeeNum; 
	}
	
	public String details()
	{
		return "This user is an employee, their name is " + firstName + " " + lastName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((employeeID == null) ? 0 : employeeID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BankEmployee other = (BankEmployee) obj;
		if (employeeID == null) {
			if (other.employeeID != null)
				return false;
		} else if (!employeeID.equals(other.employeeID))
			return false;
		return true;
	}

	public String getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(String employeeID) {
		this.employeeID = employeeID;
	}
}
