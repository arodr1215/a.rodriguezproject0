package dev.rodriguez.daos;

import java.util.List;
import dev.rodriguez.models.*;

public interface BankUserDao
{
	public List<BankUser> getAllUsers();
	public List<BankUser> getCustomerBankAccounts();
	public BankAccount createNewBankAccount(BankAccount anAccount);
	public List<BankAccount> getallBankAccounts();
	
}
