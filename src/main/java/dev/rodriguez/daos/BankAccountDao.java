package dev.rodriguez.daos;

import dev.rodriguez.models.*;

public interface BankAccountDao 
{
	public boolean decideAccount(BankUser aUser);
	public boolean withdrawMoney(BankUser aUser);
	public boolean depositMoney(BankUser aUser);
	public void viewLog();
	public boolean removeEmployeeAccount(BankUser aUser);
}
