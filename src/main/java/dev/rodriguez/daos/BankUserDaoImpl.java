package dev.rodriguez.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;


import dev.rodriguez.models.*;
import dev.rodriguez.utilities.ConnectionsUtil;


public class BankUserDaoImpl implements BankUserDao
{
	private static Logger log = Logger.getRootLogger();

	@Override
	public List<BankUser> getAllUsers() {
		log.info("Getting all bank user items");
		try (Connection connection = ConnectionsUtil.getConnection();
				Statement statement = connection.createStatement();)
		{
			
			ResultSet resultSet = statement.executeQuery("SELECT * FROM BANK_USER"); 
			List<BankUser> items = processBankUserResultSet(resultSet);
			log.info("got "+items.size()+ " from db");
			return items;
		} catch (SQLException e) {
			log.error("Exception was thrown: " + e.getClass());
			e.printStackTrace();
		} 
		return null;
	}
	
	@Override
	public List<BankAccount> getallBankAccounts()
	{
	List<BankAccount> accounts = new ArrayList<>();
	try(Connection connection = ConnectionsUtil.getConnection();
			Statement orderPrepared = connection.createStatement();){

		ResultSet rs = orderPrepared.executeQuery("select * from {oj bank_account left outer join "
				+ "bank_user_bank_account on (bank_account.id=bank_user_bank_account.account_id) left outer join "
				+ "bank_user on (bank_user_bank_account.id_user=bank_user.id_user) }");

		while(rs.next()) { // for each record
			int accountId = rs.getInt("ACCOUNT_ID");
			boolean existsAccount = false;
			for(BankAccount anAccount: accounts) {
				if(accountId==anAccount.getAccountID()) {
					existsAccount = true;

					BankUser user = (BankUser) processBankUserResultSet(rs);
					anAccount.getAccounts().add();

					break; 
				} 
			}
			if(!existsAccount) {
				
				BankAccount bankaccounts = accountFactory(rs);
				BankUser user = (BankUser) processBankUserResultSet(rs);
				if(user!=null) {
					bankaccounts.getAccounts().add(user.);
				}

				accounts.add(bankaccounts);	
			}
		
			public BankAccount accountFactory(ResultSet rs) throws SQLException {
				BankAccount bankaccounts = new BankAccount();
				bankaccounts.setAccountID(rs.getInt("ACCOUNT_ID"));
				bankaccounts.setBalance(rs.getString("BANK_BALANCE"));
				bankaccounts.setType(rs.getString("ACCOUNT_TYPE"));
				boolean isValidStatus = Arrays.stream(())
						.map((value)->value.toString())
						.anyMatch((str)->str.equals(statusString)); // checking to see if the value is found in the enum before parsing
				if(isValidStatus) {
				

		

		}

	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	}

	public BankAccount createNewBankAccount(BankAccount anAccount)  
	{
		String accountSQL = "INSERT INTO BANK_ACCOUNT (ACCOUNT_ID, BANK_BALANCE, ACCOUNT_TYPE) VALUES (?,?,?)";
		String idSQL = "SELECT MAX(ID) FROM BANK_ACCOUNT";
		String bankuserSQL = "INSERT INTO BANK_USER_BANK_ACCOUNT (ID_USER, ACCOUNT_ID) VALUES (?, ?) ";

		Connection connection = null;
		try{
			connection = ConnectionsUtil.getConnection();
			try(PreparedStatement orderPStatement = connection.prepareStatement(accountSQL);
					Statement statement = connection.createStatement();
					PreparedStatement itemPStatement = connection.prepareStatement(bankuserSQL);){

				connection.setAutoCommit(false); //this creates a user
				orderPStatement.setInt(1, anAccount.getAccountID());
				orderPStatement.setDouble(2, anAccount.getBalance());
				orderPStatement.setString(3, anAccount.getType());
				orderPStatement.execute();

				ResultSet rs = statement.executeQuery(idSQL);
				if(rs.next()) {
					int accountID = rs.getInt(1);	
					for(BankAccount accounts: anAccount.getAccounts()) {
						itemPStatement.setInt(1, accountID);
						itemPStatement.setDouble(2, accounts.getBalance());
						itemPStatement.setString (3, accounts.getType());
						itemPStatement.executeUpdate();
					}
				}
				connection.commit();
			}
		} catch (SQLException e) {
			if(connection!=null) {
				try {
					connection.rollback();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			e.printStackTrace();
		} 
		finally {
			if(connection!=null) {
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return anAccount;
	}
	
	private List<BankUser> processBankUserResultSet(ResultSet resultSet) throws SQLException
	{
		List<BankUser> items = new ArrayList<>();
		while(resultSet.next()) {
			String userType = resultSet.getString("SUB_CATEGORY");

			switch(userType) {
			case "1":
				BankCustomer customer = new BankCustomer();
				customer.setSocialSecurity(resultSet.getString("SOCIAL_SECURITY"));
				customer.setUserID(resultSet.getInt("ID_USER"));
				customer.setFirstName(resultSet.getString("FIRST_NAME"));
				customer.setLastName(resultSet.getString("LAST_NAME"));
				items.add(customer);
				break;
				
				
			case "2":
				BankEmployee employee = new BankEmployee();
				employee.setEmployeeID(resultSet.getString("EMPLOYEE_ID"));
				employee.setUserID(resultSet.getInt("ID_USER"));
				employee.setFirstName(resultSet.getString("FIRST_NAME"));
				employee.setLastName(resultSet.getString("LAST_NAME"));
				items.add(employee);
				break;
			
			default:
				System.out.println("Error with type..");
				items.add(null);
				break;
			}
		}
			return items;
	}
	
}
